#include "SDM.h"

int main(int argc, char** argv)
{
    ros::init(argc, argv, "stopping_distance_monitor");
    StoppingDistanceMonitor node;
    node.run();
    return 0;
}
