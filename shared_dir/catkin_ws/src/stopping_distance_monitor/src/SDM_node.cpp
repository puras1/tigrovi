#include "SDM.h"

StoppingDistanceMonitor::StoppingDistanceMonitor() :
    nh(),
    private_nh("~")
{
    private_nh.param<double>("min_height", min_height, 0.3);
    private_nh.param<double>("max_height", max_height, 1.5);
    private_nh.param<double>("lane_width", lane_width, 3.0);
    private_nh.param<double>("max_distance", max_distance, 10.0);
    private_nh.param<double>("max_vehicle_deceleration", max_vehicle_deceleration, -7.0);
    private_nh.param<std::string>("output_coordinate_frame", output_coordinate_frame, "base_link");

    stopping_distance_pub = nh.advertise<visualization_msgs::Marker>("/stopping_distance", 1);
    free_space_pub = nh.advertise<visualization_msgs::Marker>("/free_space", 1);
    text_pub = nh.advertise<visualization_msgs::Marker>("/text_info", 1);

    points_raw_sub = nh.subscribe("/points_raw", 1, &StoppingDistanceMonitor::pointsRawCallback, this);
    estimated_vel_sub = nh.subscribe("/estimated_vel", 1, &StoppingDistanceMonitor::estimatedVelCallback, this);
}

void StoppingDistanceMonitor::run()
{
    ros::Rate rate(10);

    while (ros::ok())
    {
        getTransformation();
        publishTextInfo();
        ros::spinOnce();
        rate.sleep();
    }
}

void StoppingDistanceMonitor::estimatedVelCallback(const geometry_msgs::Vector3Stamped& vs_msg)
{
    velocity = vs_msg.vector.x;
    stopping_distance = calculateStoppingDistance(velocity);

    visualization_msgs::Marker marker;

    marker.header.frame_id = "base_link";
    marker.header.stamp = ros::Time();
    marker.ns = "my_namespace";
    marker.id = 0;
    marker.type = visualization_msgs::Marker::CUBE;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = offset_x + stopping_distance / 2;
    marker.pose.position.y = 0;
    marker.pose.position.z = 0.2;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;

    marker.scale.x = stopping_distance;
    marker.scale.y = lane_width;
    marker.scale.z = 0.2;
    marker.color.a = 0.6; // Don't forget to set the alpha!
    marker.color.r = (stopping_distance >= obstacle_border) ? 1.0 : 0.0;
    marker.color.g = (stopping_distance >= obstacle_border) ? 0.0 : 1.0;
    marker.color.b = 0.0;

    stopping_distance_pub.publish(marker);
}

double min(double a, double b)
{
    return (a <= b) ? a : b;   
}

void StoppingDistanceMonitor::pointsRawCallback(const sensor_msgs::PointCloud2& pc_msg)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr input_pcl(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr translated_pcl(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_pcl(new pcl::PointCloud<pcl::PointXYZ>);

    pcl::fromROSMsg(pc_msg, *input_pcl);

    pcl_ros::transformPointCloud(*input_pcl, *translated_pcl, transform);

    pcl::ConditionAnd<pcl::PointXYZ>::Ptr range_cond (new pcl::ConditionAnd<pcl::PointXYZ> ());
    range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZ> ("x", pcl::ComparisonOps::GT, 1.0 + offset_x)));
    range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZ> ("x", pcl::ComparisonOps::LT, max_distance + offset_x)));
    range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZ> ("y", pcl::ComparisonOps::GT, -lane_width/2)));
    range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZ> ("y", pcl::ComparisonOps::LT, lane_width/2)));
    range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZ> ("z", pcl::ComparisonOps::GT, min_height)));
    range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZ> ("z", pcl::ComparisonOps::LT, max_height)));

    pcl::ConditionalRemoval<pcl::PointXYZ> condrem;
    condrem.setCondition(range_cond);
    condrem.setInputCloud(translated_pcl);
    condrem.filter(*filtered_pcl);

    obstacle_border = max_distance;

    for(std::size_t i=0; i < filtered_pcl->points.size(); i++)
    {
        if (filtered_pcl->points[i].x < obstacle_border)
        {
            obstacle_border = filtered_pcl->points[i].x;
        }
        ROS_INFO("Obstacle %f, current point %f", obstacle_border, filtered_pcl->points[i].x);
    }

    if (filtered_pcl->points.size() && obstacle_border > offset_x)
    {
        obstacle_border -= offset_x;
    }


    visualization_msgs::Marker marker;

    marker.header.frame_id = "base_link";
    marker.header.stamp = ros::Time();
    marker.ns = "my_namespace";
    marker.id = 0;
    marker.type = visualization_msgs::Marker::CUBE;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = offset_x + min(obstacle_border, max_distance) / 2;
    marker.pose.position.y = 0;
    marker.pose.position.z = 0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;

    marker.scale.x = min(obstacle_border, max_distance);
    marker.scale.y = lane_width;
    marker.scale.z = 0.2;
    marker.color.a = 0.6; // Don't forget to set the alpha!
    marker.color.r = 0.0;
    marker.color.g = 0.0;
    marker.color.b = 1.0;

    free_space_pub.publish(marker);
}

tf::StampedTransform StoppingDistanceMonitor::getTransformation(void)
{
    try {
        listener.waitForTransform("base_link", "velodyne",  
                                ros::Time(0), ros::Duration(3.0));
        listener.lookupTransform("base_link", "velodyne",  
                                ros::Time(0), transform);
    }
    catch (tf::TransformException ex) {
        ROS_ERROR("%s",ex.what());
        ros::Duration(1.0).sleep();
    }

    return transform;
}

void StoppingDistanceMonitor::publishTextInfo(void)
{
    visualization_msgs::Marker marker;

    marker.header.frame_id = output_coordinate_frame;
    marker.header.stamp = ros::Time();
    marker.ns = "my_namespace";
    marker.id = 0;
    marker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
    marker.action = visualization_msgs::Marker::ADD;

    marker.pose.position.x = 10.0;
    marker.pose.position.y = 10.0;
    marker.pose.position.z = 0;

    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;

    marker.scale.x = 0.0;
    marker.scale.y = 0.0;
    marker.scale.z = 1.0;

    marker.color.a = 1.0; // Don't forget to set the alpha!
    marker.color.r = 1.0;
    marker.color.g = 1.0;
    marker.color.b = 1.0;

    marker.text = "Velocity: " + boost::to_string(velocity) + "\nStopping distance: "+ boost::to_string(stopping_distance) + "\nFree space: " + boost::to_string(obstacle_border);
    text_pub.publish(marker);
}



double StoppingDistanceMonitor::calculateStoppingDistance(double velocity)
{
    double previous_vel = 0;
    double average_vel = (velocity+previous_vel)/2;
    previous_vel = average_vel;

    stopping_distance = -average_vel*average_vel/2/max_vehicle_deceleration;
    return stopping_distance;
}