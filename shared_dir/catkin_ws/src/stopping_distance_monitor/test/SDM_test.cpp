#include "SDM.h"
#include <gtest/gtest.h>
#include <cmath>

TEST(TestStoppingDistance, stopping_distance)
{
	StoppingDistanceMonitor sdm;
	ASSERT_EQ(2.0, round(sdm.calculateStoppingDistance(10.0)));
}

int main(int argc, char **argv){
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "tester");
  return RUN_ALL_TESTS();
}
