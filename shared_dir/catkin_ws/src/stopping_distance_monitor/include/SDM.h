#ifndef STOPPING_DISTANCE_MONITOR_H
#define STOPPING_DISTANCE_MONITOR_H

#include <ros/ros.h>
#include "geometry_msgs/Vector3Stamped.h"
#include "sensor_msgs/PointCloud2.h"
#include <tf/transform_datatypes.h>
#include <visualization_msgs/Marker.h>
#include <tf/transform_listener.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <pcl/filters/conditional_removal.h>
#include "std_msgs/String.h"
#include <geometry_msgs/Vector3.h>
#include <pcl_ros/point_cloud.h>
#include <tf/tf.h>
#include <pcl_ros/transforms.h>
class StoppingDistanceMonitor
{
private:

    // nodehandle
    ros::NodeHandle nh;
    ros::NodeHandle private_nh;

    // ros publisher
    ros::Publisher stopping_distance_pub;
    ros::Publisher free_space_pub;
    ros::Publisher text_pub;
    
    // ros subscribers
    ros::Subscriber points_raw_sub;
    ros::Subscriber estimated_vel_sub;

    //ROI parameters
    double min_height;
    double max_height;
    double lane_width;
    double max_distance;
    double max_vehicle_deceleration;

    const double offset_x = 4.0;
    double obstacle_border;
    double velocity;
    double stopping_distance;

    std::string output_coordinate_frame;
    tf::StampedTransform transform;
    tf::TransformListener listener;

    void estimatedVelCallback(const geometry_msgs::Vector3Stamped& vs_msg);
    void pointsRawCallback(const sensor_msgs::PointCloud2& pc_msg);
    
    tf::StampedTransform getTransformation(void);
    void publishTextInfo(void);

public:

    StoppingDistanceMonitor();
    ~StoppingDistanceMonitor(){};

    double calculateStoppingDistance(double velocity);

    void run();
};


#endif  // STOPPING_DISTANCE_MONITOR_H